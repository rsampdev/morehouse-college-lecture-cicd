package main

import (
  "strings"
  "testing"
  "github.com/blang/semver/v4"
)

func TestGetTanuki(t *testing.T) {
  //Simple test, check the included color codes and a typical string.
  tables := []string{ "\033[38;5;208m", "-ooooooooo-++++++++++++++-ooooooooo-" }

  res := GetTanuki(true)
  for _, table := range(tables) {
    if strings.Contains(res, table) != true {
      t.Errorf("GetTanuki failed at checking for '%s'.", table)
    }
  }
}

func TestGetVersion(t *testing.T) {
    _, err := semver.Parse(GetVersion())

    if err != nil {
        t.Errorf("GetVersion failed at checking for semantic version.")
    }
}
